package Slider;
import controlador.OyenteSlider;
import javax.swing.JFrame;
import vista.PanelSlider;



public class Slider {

  public static void main(String[] args) {
    PanelSlider panel = new PanelSlider();
    OyenteSlider oyente  = new OyenteSlider(panel);
    panel.addEventos(oyente);
    JFrame f = new JFrame("Kilometros y Millas");
    f.setSize(500,300);
    f.setLocation(100,100);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.add(panel);
    f.setVisible(true);
  }
  
}


