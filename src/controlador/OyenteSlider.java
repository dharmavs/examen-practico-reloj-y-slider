package controlador;

import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import vista.PanelSlider;

public class OyenteSlider implements ChangeListener{
  private PanelSlider panel;
  
  public OyenteSlider(PanelSlider panel) {
    this.panel = panel;
  }

  @Override
  public void stateChanged(ChangeEvent e) {
    Component origen = (Component)e.getSource();
    switch(origen.getName()){
      case "sliderKm":{
        int prueba = panel.getSliderKm().getValue();
        panel.getEtiquetaKm().setText(prueba + " kms / h");
        int kmAMilla = (int)Math.floor(prueba / 1.609);
        panel.getEtiquetaM().setText(kmAMilla + " mph");
        panel.getSliderM().setValue(kmAMilla);
        break;
      }
      
      case "sliderM":{
        int prueba = panel.getSliderM().getValue();
        panel.getEtiquetaM().setText(prueba + " mph");
        int millaAKm = (int)Math.ceil(prueba * 1.609);
        panel.getEtiquetaKm().setText(millaAKm + " kms / h");
        panel.getSliderKm().setValue(millaAKm);
        break;
      }
    }
  }
  
}

